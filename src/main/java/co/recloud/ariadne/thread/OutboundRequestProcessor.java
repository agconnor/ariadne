package co.recloud.ariadne.thread;

import co.recloud.ariadne.queue.OutboundRequestQueue;
import co.recloud.ariadne.request.DataRequest;
import co.recloud.ariadne.request.Request;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/16/11
 * Time: 11:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class OutboundRequestProcessor implements Runnable {
    public void run() {
        while(!Main.waitForServiceStatus()) {
            continue;
        }
        BlockingQueue<Request> requestQueue = OutboundRequestQueue.getQueue();
        while(true) {
            try {
                Request request = requestQueue.take();
                request.sendAsync(request.getTarget());
            } catch (InterruptedException e) {
                synchronized (this) {
                    try {
                        wait(10);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            } finally {

            }
        }
    }
}
