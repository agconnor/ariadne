package co.recloud.ariadne.thread;

import co.recloud.ariadne.config.Configuration;
import co.recloud.ariadne.server.DataServer;
import co.recloud.ariadne.store.HostTable;
import co.recloud.ariadne.store.DataStore;

import java.util.Collection;
import java.util.Set;

public class CondenserThread implements Runnable {

    public void run() {
        while (true) {
            for (int x = 0; x < Configuration.DATA_FILE_WIDTH; x++) {
                for (int y = 0; y < Configuration.DATA_FILE_HEIGHT; y++) {
                    if (!Main.waitForServiceStatus()) {
                        continue;
                    }
                    if (!Main.waitForIdle()) {
                        continue;
                    }
                    HostTable ht = HostTable.getInstance();
                    Collection<DataStore> stores = DataStore.getAllStores();
                    for(DataStore store : stores) {
                        for (String path : store.getAllPaths(x, y)) {
                            Main.waitForIdle();
                            store.condensePath(path);
                        }
                    }
                    DataServer.condenseCommittedTimes();
                }
            }
        }
    }

}
