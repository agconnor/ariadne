package co.recloud.ariadne.thread;

import co.recloud.ariadne.model.logical.Transaction;
import co.recloud.ariadne.queue.OutboundRequestQueue;
import co.recloud.ariadne.queue.TransactionQueue;
import co.recloud.ariadne.request.Request;

import java.util.concurrent.BlockingQueue;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/23/11
 * Time: 9:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionProcessor implements Runnable {
    public void run() {
        while(!Main.waitForServiceStatus()) {
            continue;
        }
        BlockingQueue<Transaction> commitQueue = TransactionQueue.getQueue();
        while(true) {
            try {
                synchronized (this) {
                    try {
                        wait(10);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                Transaction transaction = commitQueue.take();
                transaction.commit();
            } catch (InterruptedException e) {
                synchronized (this) {
                    try {
                        wait(10);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            } finally {

            }
        }
    }
}
