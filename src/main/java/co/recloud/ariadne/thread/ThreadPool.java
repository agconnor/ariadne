package co.recloud.ariadne.thread;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
	private static ThreadPoolExecutor apiExecutor = new ThreadPoolExecutor(1000, 2000, 5000L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	private static ThreadPoolExecutor dataExecutor = new ThreadPoolExecutor(2000, 3000, 5000L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
    public static void startApiThread(Runnable task) {
		apiExecutor.execute(task);
	}
    public static void startDataThread(Runnable task) {
        dataExecutor.execute(task);
    }
}
