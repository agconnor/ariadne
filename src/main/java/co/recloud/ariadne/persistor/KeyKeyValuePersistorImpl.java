package co.recloud.ariadne.persistor;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Transaction;
import co.recloud.ariadne.store.TransactionCache;

/**
 * 
 * @author alex
 *
 */
public class KeyKeyValuePersistorImpl implements KeyKeyValuePersistor {
	
	private Transaction transaction;
//	private Host host = null;

	public void setHost(Host host) {
//		this.host = host;
	}

	public KeyKeyValuePersistorImpl(Transaction transaction) {
		this.transaction = transaction;
	}
	/**
	 * Create a key
	 * @param table
	 * @param key
	 * @return
	 */
	private String createKey(String table, String key) {
		return table + ":" + key;
	}
	
	/**
	 * Extract an ID from a key
	 * @param key
	 * @return
	 */
	private String extractID(String key) {
		String result = null;
		if(key != null) {
			String[] tokens = key.split(":");
			if(tokens.length > 1) {
				result = tokens[1];
			}
		}
		return result;
	}

	public void deleteParentToChild(Column parentColumn, String parentKey, Column childColumn, String childKey) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		String childTable = childColumn.getTable().getName();
		String childField = childColumn.getField();
		String parentTable = parentColumn.getTable().getName();
		pool.deleteColumn(transaction, "kkv_p_c", childTable + "." + childField, createKey(parentTable, parentKey), createKey(childTable, childKey));
	}

	public void setParentToChild(Column parentColumn, String parentKey, Column childColumn, String childKey) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		String childTable = childColumn.getTable().getName();
		String childField = childColumn.getField();
		String parentTable = parentColumn.getTable().getName();
		pool.putColumn(transaction, "kkv_p_c", childTable + "." + childField, createKey(parentTable, parentKey), createKey(childTable, childKey), new Long(1));
	}

	public Set<String> getParentToChild(Column parentColumn, String parentKey, Column childColumn) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		String childTable = childColumn.getTable().getName();
		String childField = childColumn.getField();
		String parentTable = parentColumn.getTable().getName();
		Map<String, Long> childKeys = pool.getAllLongs(transaction, "kkv_p_c", childTable + "." + childField, createKey(parentTable, parentKey), false);
		Set<String> results = new HashSet<String>();
		if(childKeys != null) {
			Long one = new Long(1);
			for(String childKey : childKeys.keySet()) {
				String childId = extractID(childKey);
				if(childKeys.get(childKey) != null && childKeys.get(childKey).equals(one)) {
					results.add(childId);
				}
			}
		}
		return results;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Transaction getTransaction() {
		return transaction;
	}
}
