package co.recloud.ariadne.persistor;

import java.util.Map;
import java.util.Set;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Table;
import co.recloud.ariadne.model.logical.Transaction;

/**
 * Persists key-values
 * @author alex
 *
 */
public interface KeyValuePersistor {
	public String getValue(String key, Column column);
	
	public void setHost(Host host);

	public void setValue(String key, Column column, String value);
	
	public int estimateCardinality(Table table);
	
	public Set<String> getAllKeys(Table table);

	public Map<String, Object> getAllColumns(String key, Table table);
	
	public Map<String, Object> getColumns(String key, Set<Column> columns);
	
	public void dropTable(Table table);
	
	public void setTransaction(Transaction transaction);
	
	public Transaction getTransaction();
}
