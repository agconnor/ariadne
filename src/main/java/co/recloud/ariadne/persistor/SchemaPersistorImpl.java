/**
 * 
 */
package co.recloud.ariadne.persistor;

import java.util.List;

import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Table;
import co.recloud.ariadne.model.logical.Transaction;
import co.recloud.ariadne.store.TransactionCache;

/**
 * @author alex
 * 
 */
public class SchemaPersistorImpl implements SchemaPersistor {
	private Transaction transaction;

	public SchemaPersistorImpl(Transaction transaction) {
		this.transaction = transaction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see co.recloud.ariadne.persistor.SchemaPersistor#link(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	public void link(Column childColumn, Table parentTable) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		pool.putColumn(transaction, "schema", "schema_c_p", childColumn
				.getTable().getName(), childColumn.getField(), parentTable
				.getName());
		pool.putColumn(transaction, "schema", "schema_p_c",
				parentTable.getName(), childColumn.getTable().getName(),
				childColumn.getField());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.recloud.ariadne.persistor.SchemaPersistor#unlink(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	public void unlink(Column childColumn, Table parentTable) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		pool.deleteColumn(transaction, "schema", "schema_c_p", childColumn
				.getTable().getName(), childColumn.getField());
		pool.deleteColumn(transaction, "schema", "schema_p_c",
				parentTable.getName(), childColumn.getTable().getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.recloud.ariadne.persistor.SchemaPersistor#getParentTable(java.lang
	 * .String, java.lang.String)
	 */
	public Table getParentTable(Column childColumn) {
		TransactionCache pool = TransactionCache.getInstance(transaction.getId());
		Table result = null;
		String tableName = pool.getString(transaction, "schema", "schema_c_p",
				childColumn.getTable().getName(), childColumn.getField(), false);
		if (tableName != null) {
			result = Table.parseTable(tableName, null);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * co.recloud.ariadne.persistor.SchemaPersistor#getChildFields(java.lang
	 * .String, java.lang.String)
	 */
	public List<Column> getChildFields(Table parentTable, Table childTable) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setLocalHostName(String localhost) {
		// TODO Auto-generated method stub

	}

}
