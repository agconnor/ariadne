package co.recloud.ariadne.persistor;

import java.util.Set;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Table;
import co.recloud.ariadne.model.logical.Transaction;

public interface KeyKeyValuePersistor {
	
	/**
	 * 
	 * @param host
	 */
	public void setHost(Host host);

	/**
	 * 
	 * @param parentTable
	 * @param oldParentKey
	 * @param table
	 * @param field
	 * @param key
	 */
	public void deleteParentToChild(Column parentColumn, String parentKey, Column childColumn, String childKey);

	/**
	 * 
	 * @param parentTable
	 * @param parentKey
	 * @param table
	 * @param field
	 * @param key
	 */
	public void setParentToChild(Column parentColumn, String parentKey, Column childColumn, String childKey);

	/**
	 * 
	 * @param key
	 * @param table
	 * @param field
	 * @return
	 */
	public Set<String> getParentToChild(Column parentColumn, String parentKey, Column childColumn);
	
	public void setTransaction(Transaction transaction);
	
	public Transaction getTransaction();
}
