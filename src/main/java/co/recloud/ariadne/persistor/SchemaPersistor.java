/**
 * 
 */
package co.recloud.ariadne.persistor;

import java.util.List;

import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Table;

/**
 * Interface for working with the schema
 * 
 * @author alex
 *
 */
public interface SchemaPersistor{
	/**
	 * Link a child table and field to a parent table (always to key).
	 * 
	 * @param childTable
	 * @param childField
	 * @param parentTable
	 */
	public void link(Column childColumn, Table parentTable);
	
	/**
	 * Unlink a child table, field and parent table
	 * 
	 * @param childTable
	 * @param childField
	 * @param parentTable
	 */
	public void unlink(Column childColumn, Table parentTable);
	
	/**
	 * Get the parent table, given a child table and field
	 * 
	 * @param childTable
	 * @param childField
	 * @return
	 */
	public Table getParentTable(Column childColumn);
	
	/**
	 * Get the child fields for a given parent table and child table
	 * 
	 * @param parentTable
	 * @param childTable
	 * @return
	 */
	public List<Column> getChildFields(Table parentTable, Table childTable);
	
	/**
	 * 
	 * @param localhost
	 */
	public void setLocalHostName(String localhost);
}
