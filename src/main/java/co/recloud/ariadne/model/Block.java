package co.recloud.ariadne.model;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.KeyField;
import com.sleepycat.persist.model.PrimaryKey;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 4/28/12
 * Time: 9:14 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
public class Block {

    @PrimaryKey
    public Integer coord;

    public Map<String, SortedMap<Long, SortedMap<Long, Map<String, Object>>>> block;

}
