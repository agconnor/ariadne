package co.recloud.ariadne.model.plan;

import co.recloud.ariadne.model.logical.Transaction;

public class FullScan extends Operator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7151430841700254898L;

	public FullScan(Transaction transaction) {
		super(transaction);
		// TODO Auto-generated constructor stub
	}

	public String toString() {
		return "FullScan: on " + this.getTable().getAlias();
	}
}
