package co.recloud.ariadne.model.plan;

import co.recloud.ariadne.model.logical.Column;
import co.recloud.ariadne.model.logical.Transaction;
import co.recloud.ariadne.store.TransactionCache;

public class Link extends Operator {

	public static final int CHILD_TO_PARENT = 1;
	public static final int PARENT_TO_CHILD = 2;

	private Column baseColumn;
	private int step;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1386948696446261490L;

	public Link(Transaction transaction) {
		super(transaction);
	}

	public void execute(String localhost, int port) {
		TransactionCache pool = TransactionCache.getInstance(this.getTransaction().getId());
		pool.putColumn(this.getTransaction(), "schema", "schema_c_p",
				baseColumn.getTable().getName(), baseColumn.getField(), this
						.getTable().getName());
		pool.putColumn(this.getTransaction(), "schema", "schema_p_c", this
				.getTable().getName(), baseColumn.getTable().getName(),
				baseColumn.getField());
	}

	/**
	 * @param baseColumn
	 *            the baseColumn to set
	 */
	public void setBaseColumn(Column baseColumn) {
		this.baseColumn = baseColumn;
	}

	/**
	 * @return the baseColumn
	 */
	public Column getBaseColumn() {
		return baseColumn;
	}

	/**
	 * @param step
	 *            the step to set
	 */
	public void setStep(int step) {
		this.step = step;
	}

	/**
	 * @return the step
	 */
	public int getStep() {
		return step;
	}
}
