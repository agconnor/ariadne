package co.recloud.ariadne.model;

public class Address {
	private String key;
	
	private Host host;

	public void setHost(Host host) {
		this.host = host;
	}

	public Host getHost() {
		return host;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}
