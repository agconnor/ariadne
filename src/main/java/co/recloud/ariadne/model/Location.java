package co.recloud.ariadne.model;

import java.io.Serializable;

public class Location implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1225960483257662145L;
	private Long x;
	private Long y;

	public Long getX() {
		return x;
	}
	public void setX(Long x) {
		this.x = x;
	}
	public Long getY() {
		return y;
	}
	public void setY(Long y) {
		this.y = y;
	}
	public String toString() {
		return x + "," + y;
	}
	public int hashCode() {
		return toString().hashCode();
	}
	public boolean equals(Object that) {
		if(that instanceof Location) {
			Location thatLocation = (Location) that;
			if(((this.x == null && thatLocation.x == null) || (this.x != null && this.x.equals(thatLocation.x))) && 
					((this.y == null && thatLocation.y == null) || (this.y != null && this.y.equals(thatLocation.y)))) {
				return true;
			}
		}
		return false;
	}
}
