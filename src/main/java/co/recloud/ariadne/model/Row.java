package co.recloud.ariadne.model;

import java.util.Set;
import java.util.Map;

public class Row {
	private Map<String, Cell> data;
	
	public Object getValue(String field) {
		Cell cell = data.get(field);
		return cell.getValue();
	}
	
	@SuppressWarnings("rawtypes")
	public Class getType(String field) {
		return data.get(field).getType();
	}
	
	public Set<String> getFields() {
		return data.keySet();
	}
}
