package co.recloud.ariadne.model;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 12/18/11
 * Time: 1:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class MultiLevelMap {
    private Map<Object, MultiLevelMap> baseMap;
    private SortedMap<Object, MultiLevelMap> baseSortedMap;
    private ConcurrentMap<Object, MultiLevelMap> baseConcurrentMap;
    private ConcurrentNavigableMap<Object, MultiLevelMap> baseConcurrentOrderedMap;
    private Object baseValue;
    public enum Type {
        MAP,
        SORTED,
        CONCURRENT,
        CONCURRENT_ORDERED,
        VALUE
    };
    private Type type;
    private List<Type> path;
    private Object singleValue;
    private MultiLevelMap nextLevel;
    private Object key;
    
    protected MultiLevelMap(Type type) {
        path = new LinkedList<Type>();
    }
    
    public MultiLevelMap build(Type type) {
        this.type = type;

        switch (type) {
            case MAP:
                baseMap = new HashMap<Object, MultiLevelMap>();
                break;
            case SORTED:
                baseSortedMap = new TreeMap<Object, MultiLevelMap>();
                baseMap = baseSortedMap;
                break;
            case CONCURRENT:
                baseConcurrentMap = new ConcurrentHashMap<Object, MultiLevelMap>();
                baseMap = baseConcurrentMap;
                break;
            case CONCURRENT_ORDERED:
                baseConcurrentOrderedMap = new ConcurrentSkipListMap<Object, MultiLevelMap>();
                baseConcurrentMap = baseConcurrentOrderedMap;
                baseSortedMap = baseConcurrentOrderedMap;
                baseMap = baseConcurrentOrderedMap;
                break;
        }
        return this;
    }
    
    public MultiLevelMap get(Object key) {
        return null;
    }
    
    public Object value() {
        return baseValue;
    }
    
    public MultiLevelMap value(Object value) {
        this.baseValue = value;
        return this;
    }
    
    public MultiLevelMap set(Object key) {
        if(!baseMap.containsKey(key)) {
            baseMap.put(key, null);
        }
        return this;
    }
}        
