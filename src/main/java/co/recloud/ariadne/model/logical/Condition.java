package co.recloud.ariadne.model.logical;

import java.io.Serializable;

public class Condition implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4589998208124279527L;
	private Column leftColumn;
	private String rightValue;
	private Column rightColumn;
	/**
	 * @return the leftColumn
	 */
	public Column getLeftColumn() {
		return leftColumn;
	}
	/**
	 * @param leftColumn the leftColumn to set
	 */
	public void setLeftColumn(Column leftColumn) {
		this.leftColumn = leftColumn;
	}
	/**
	 * @return the rightValue
	 */
	public String getRightValue() {
		return rightValue;
	}
	/**
	 * @param rightValue the rightValue to set
	 */
	public void setRightValue(String rightValue) {
		this.rightValue = rightValue;
	}
	/**
	 * @return the rightColumn
	 */
	public Column getRightColumn() {
		return rightColumn;
	}
	/**
	 * @param rightColumn the rightColumn to set
	 */
	public void setRightColumn(Column rightColumn) {
		this.rightColumn = rightColumn;
	}
	
	public String toString() {
		return leftColumn + " = " + (rightColumn != null ? rightColumn : rightValue);
	}
}
