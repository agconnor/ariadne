package co.recloud.ariadne.model.logical;

import java.util.Set;

import co.recloud.ariadne.model.Host;

/**
 * Root model for commands
 * @author alex
 *
 */
public class Command {
	public static final int SELECT = 0;
	public static final int UPDATE = 1;
	public static final int LINK = 2;
	public static final int UNLINK = 3;
	public static final int DELETE = 4;
	public static final int BEGIN = 6;
	public static final int COMMIT = 7;
	public static final int ABORT = 8;
	public static final int QUIT = 5;
	public static final int ADD_HOST = 9;
	
	private Table table;
	private int commandType;
	private Condition condition;
	private Set<Column> columns;
	private Set<Assignment> assignments;
	private Host host;
	
	/**
	 * @return the commandType
	 */
	public int getCommandType() {
		return commandType;
	}
	/**
	 * @param commandType the commandType to set
	 */
	public void setCommandType(int commandType) {
		this.commandType = commandType;
	}
	/**
	 * 
	 * @param condition
	 */
	public void setCondition(Condition condition) {
		this.condition = condition;
	}
	/**
	 * 
	 * @return
	 */
	public Condition getCondition() {
		return condition;
	}
	/**
	 * @param columns the columns to set
	 */
	public void setColumns(Set<Column> columns) {
		this.columns = columns;
	}
	/**
	 * @return the columns
	 */
	public Set<Column> getColumns() {
		return columns;
	}
	/**
	 * @param assignments the assignments to set
	 */
	public void setAssignments(Set<Assignment> assignments) {
		this.assignments = assignments;
	}
	/**
	 * @return the assignments
	 */
	public Set<Assignment> getAssignments() {
		return assignments;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}
	/**
	 * @return the table
	 */
	public Table getTable() {
		return table;
	}
	/**
	 * @param host the host to set
	 */
	public void setHost(Host host) {
		this.host = host;
	}
	/**
	 * @return the host
	 */
	public Host getHost() {
		return host;
	}
}
