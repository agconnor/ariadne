package co.recloud.ariadne.model.logical;

import java.io.Serializable;

public class Table implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5931590730440524567L;
	public static final String PRIMARY_KEY = "id";
	private String name;
	private String alias;
	private Join join;
	private boolean isResultSet;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the join
	 */
	public Join getJoin() {
		return join;
	}
	/**
	 * @param join the join to set
	 */
	public void setJoin(Join join) {
		this.join = join;
	}
	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * @return the alias
	 */
	public String getAlias() {
		if(alias != null) {
			return alias;
		} else {
			return name;
		}
	}
	
	public String toString() {
		return this.getName() + "{" + this.getAlias() + "}";
	}
	
	public static Table parseTable(String tableName, String tableAlias) {
		Table table = new Table();
		table.setName(tableName);
		table.setAlias(tableAlias);
		return table;
	}
	/**
	 * @param isResultSet the isResultSet to set
	 */
	public void setResultSet(boolean isResultSet) {
		this.isResultSet = isResultSet;
	}
	/**
	 * @return the isResultSet
	 */
	public boolean isResultSet() {
		return isResultSet;
	}
	
	/**
	 * Compare two tables
	 */
	public boolean equals(Object thatObject) {
		boolean result = false;
		if(thatObject instanceof Table) {
			Table that = (Table) thatObject;
			if(that.getName().equals(this.getName()) &&
					that.getAlias().equals(this.getAlias())) {
				result = true;
			}
		}
		return result;
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
}
