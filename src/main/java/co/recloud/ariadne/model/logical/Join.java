package co.recloud.ariadne.model.logical;

import java.io.Serializable;

public class Join implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2702667783317422186L;
	public static final int INNER = 0;
	public static final int LEFT = 1;
	public static final int RIGHT = 2;
	public static final int OUTER = 3;
	private int type;
	private Table table;
	private Condition condition;
	private Join join;
	/**
	 * @return the table
	 */
	public Table getTable() {
		return table;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}
	/**
	 * @return the condition
	 */
	public Condition getCondition() {
		return condition;
	}
	/**
	 * @param condition the condition to set
	 */
	public void setCondition(Condition condition) {
		this.condition = condition;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param join the join to set
	 */
	public void setJoin(Join join) {
		this.join = join;
	}
	/**
	 * @return the join
	 */
	public Join getJoin() {
		return join;
	}
	
}
