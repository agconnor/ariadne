package co.recloud.ariadne.model;

import java.io.Serializable;
import co.recloud.ariadne.config.Configuration;

public class Host implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5899154268965140048L;

	private String id;
	
	private String hostName;

	private Integer port;
	
	private Location location;
	
	private Long token;

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getPort() {
		return port;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(Long token) {
		this.token = token;
	}

	/**
	 * @return the token
	 */
	public Long getToken() {
		return token;
	}
	
	public void generateToken() {
        int fileSpace = Configuration.DATA_FILE_HEIGHT * Configuration.DATA_FILE_WIDTH;
        double max = (double) (Integer.MAX_VALUE - (Integer.MAX_VALUE % fileSpace));
        double min = (double) (Integer.MIN_VALUE - (Integer.MIN_VALUE % fileSpace));
		token = (long) ((Math.random() * (max - min) / (double) fileSpace) + min);
	}
	
	public String toString() {
		return hostName + ":" + port + ":" + token;
	}
	
	public int hashCode() {
		return (hostName + ":" + port).hashCode();
	}
	
	public boolean equals(Object that) {
		if(that instanceof Host) {
			Host thatHost = (Host) that;
			if(thatHost.getHostName().equals(this.hostName) &&
				thatHost.getPort().intValue() == port.intValue()) {
				return true;
			}
		}
		return false;
	}
}
