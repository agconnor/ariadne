package co.recloud.ariadne.model;

public class Cell {
	private String fieldName;
	private Object value;
	@SuppressWarnings("rawtypes")
	private Class type;
	/**
	 * @return the fieldName
	 */
	public String getFieldName() {
		return fieldName;
	}
	/**
	 * @param fieldName the fieldName to set
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
	/**
	 * @return the type
	 */
	@SuppressWarnings("rawtypes")
	public Class getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	@SuppressWarnings("rawtypes")
	public void setType(Class type) {
		this.type = type;
	}
	
}
