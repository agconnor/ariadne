package co.recloud.ariadne.exception;

/**
 * Exception thrown when an key's address is not resolved
 * 
 * @author alex
 *
 */
public class KeyAddressNotResolvedException extends Exception {

	/**
	 * Generated
	 */
	private static final long serialVersionUID = -5482331457314592873L;

}
