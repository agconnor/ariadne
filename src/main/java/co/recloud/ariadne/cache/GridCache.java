package co.recloud.ariadne.cache;

import co.recloud.ariadne.store.GridBlockStore;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/21/11
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GridCache<V> {
    public void setStore(GridBlockStore<V> store);
    public V get(int x, int y);
    public void put(int x, int y, V value);
    public void setDirty(int x, int y);
    public void pin(int x, int y);
    public void unpin(int x, int y);
    public byte [] getBlock(int x, int y);
    public void putBlock(int x, int y, byte [] block);
    public int[] getOpenBlock();
    public void freeBlock(int x, int y);
}
