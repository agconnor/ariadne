package co.recloud.ariadne.cache;

import co.recloud.ariadne.config.Configuration;
import co.recloud.ariadne.store.GridBlockStore;
import sun.security.krb5.Config;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/21/11
 * Time: 4:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class DataCache<V> implements GridCache<V> {

    private class Entry {
        public Entry next;
        public Entry prev;
        public V row;
        public int x;
        public int y;
        public boolean pinned;
        public boolean dirty;
    }

    private Entry[][] data;
    private int[][] sizes;
    private Long size;
    private Entry lru;
    private Entry mru;
    private int capacity;
    private GridBlockStore<V> store;
    private Lock lruChainLock;
    private Lock sizeLock;
    private Lock evictionLock;
    private Lock[][] latches;

    public DataCache(int capacity, int width, int height) {
        data = (Entry[][]) Array.newInstance(Entry.class, width, height);
        sizes = new int[width][height];
        latches = new Lock[width][height];
        lruChainLock = new ReentrantLock();
        sizeLock = new ReentrantLock();
        evictionLock = new ReentrantLock();
        for (int x = 0; x < width; x++) {
            for(int y = 0; y < width; y++) {
                sizes[x][y] = 0;
                latches[x][y] = new ReentrantLock();
            }
        }
        size = 0L;
        this.capacity = capacity;
    }

    private int getSize(V row) {
        if(row != null) {
            return row.toString().length();
        } else {
            return 0;
        }
    }

    private void unlink(Entry entry) {
        lruChainLock.lock();
        Entry before = entry.prev;
        Entry after = entry.next;
        if (entry == lru) {
            lru = after;
        }
        if (entry == mru) {
            mru = before;
        }
        if (before != null) {
            before.next = after;
        }
        if (after != null) {
            after.prev = before;
        }
        lruChainLock.unlock();
    }

    private void setToMRU(Entry entry) {
        lruChainLock.lock();
        if (mru != null) {
            mru.next = entry;
        }
        entry.prev = mru;
        entry.next = null;
        mru = entry;
        if (lru == null) {
            lru = entry;
        }
        lruChainLock.unlock();
    }

    private void checkPath(int x, int y, boolean pinned, V newBlock) {
        int oldSize = 0;
        latches[x][y].lock();
        Entry entry = data[x][y];
        if(entry == null) {
            V block;
            if(newBlock == null) {
                block = store.readBlock(x, y);
            } else {
                block = newBlock;
            }
            entry = new Entry();
            entry.x = x;
            entry.y = y;
            entry.row = block;
            entry.pinned = pinned;
            entry.dirty = false;
            data[x][y] = entry;
        }
        if(newBlock != null) {
            oldSize = getSize(entry.row);
            entry.row = newBlock;
            entry.pinned = true;
        }
        latches[x][y].unlock();
        int rowSize = getSize(entry.row);
        sizeLock.lock();
        size += rowSize - oldSize;
        sizeLock.unlock();
        unlink(entry);
        setToMRU(entry);
    }

    public void evict() {
        if(evictionLock.tryLock()) {
            while (size > capacity) {
                if (lru != null) {
                    lruChainLock.lock();
                    Entry target = lru;
                    while(target != null && target.pinned) {
                        target = target.next;
                    }
                    if(target == null) {
                        break;
                    }
                    unlink(target);
                    lruChainLock.unlock();
                    int x = target.x;
                    int y = target.y;
                    latches[x][y].lock();
                    if (store != null && target.dirty) {
                        store.writeBlock(x, y, lru.row);
                    }
                    data[x][y] = null;
                    int oldSize = sizes[x][y];
                    sizes[x][y] = 0;
                    latches[x][y].unlock();
                    sizeLock.lock();
                    size -= oldSize;
                    sizeLock.unlock();
                }
            }
            evictionLock.unlock();
        }
    }

    public void setStore(GridBlockStore<V> vGridBlockStore) {
        store = vGridBlockStore;
    }

    public V get(int x, int y) {
        V result;
        checkPath(x, y, false, null);
        Entry entry = data[x][y];
        result = entry.row;
        evict();
        return result;
    }

    public void put(int x, int y, V row) {
        int dataSize = 0;
        if (row != null) {
            dataSize = getSize(row);
        }
        checkPath(x, y, false, row);
        evict();
    }

    public void setDirty(int x, int y) {
        checkPath(x, y, false, null);
        latches[x][y].lock();
        data[x][y].dirty = true;
        latches[x][y].unlock();
    }

    public void pin(int x, int y) {
        checkPath(x, y, true, null);
        latches[x][y].lock();
        data[x][y].pinned = true;
        latches[x][y].unlock();
    }

    public void unpin(int x, int y) {
        latches[x][y].lock();
        data[x][y].pinned = false;
        latches[x][y].unlock();
    }
    
    public byte[] getBlock(int x, int y) {
        return store.readRawBlock(x, y);
    }
    
    public void putBlock(int x, int y, byte [] block) {
        store.writeRawBlock(x, y, block);
    }
    
    public int[] getOpenBlock() {
        return store.getOpenBlock();
    }

    public void freeBlock(int x, int y) {
        store.freeBlock(x, y);
    }
}
