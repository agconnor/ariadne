package co.recloud.ariadne.request;

public class KeyUpdateGossipRequest extends Request {
	/**
	 * 
	 */
	private static final long serialVersionUID = 976205876800094447L;
	private String path;
	private Long updatedTime;
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param updatedTime the updatedTime to set
	 */
	public void setUpdatedTime(Long updatedTime) {
		this.updatedTime = updatedTime;
	}
	/**
	 * @return the updatedTime
	 */
	public Long getUpdatedTime() {
		return updatedTime;
	}
}
