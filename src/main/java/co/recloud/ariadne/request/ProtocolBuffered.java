package co.recloud.ariadne.request;

import com.google.protobuf.Message;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 12/17/11
 * Time: 5:28 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ProtocolBuffered {
    public Message getMessage();
    public void loadFromMessage(Message message);
}
