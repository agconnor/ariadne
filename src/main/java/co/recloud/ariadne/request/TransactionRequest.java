package co.recloud.ariadne.request;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.logical.Transaction;
import com.google.protobuf.Message;

public class TransactionRequest extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6846428617555776046L;

	private Transaction transaction;

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
    }
}
