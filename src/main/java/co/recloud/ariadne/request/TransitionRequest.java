package co.recloud.ariadne.request;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.Location;

public class TransitionRequest extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2667209111415053225L;
	
	private int newStatus;
	private Location location;
	private Host sourceHost;
	
	/**
	 * @param newStatus the newStatus to set
	 */
	public void setNewStatus(int newStatus) {
		this.newStatus = newStatus;
	}
	/**
	 * @return the newStatus
	 */
	public int getNewStatus() {
		return newStatus;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}
	/**
	 * @param sourceHost the sourceHost to set
	 */
	public void setSourceHost(Host sourceHost) {
		this.sourceHost = sourceHost;
	}
	/**
	 * @return the sourceHost
	 */
	public Host getSourceHost() {
		return sourceHost;
	}
}
