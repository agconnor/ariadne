package co.recloud.ariadne.request;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 4/14/12
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddressPropogateRequest extends Request {
    private int baseAddress;
    private int mappedAddress;

    public int getBaseAddress() {
        return baseAddress;
    }

    public void setBaseAddress(int baseAddress) {
        this.baseAddress = baseAddress;
    }

    public int getMappedAddress() {
        return mappedAddress;
    }

    public void setMappedAddress(int mappedAddress) {
        this.mappedAddress = mappedAddress;
    }
}
