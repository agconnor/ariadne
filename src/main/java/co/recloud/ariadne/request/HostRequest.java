package co.recloud.ariadne.request;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.store.HostTable;

public class HostRequest extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5230390720928559547L;
	private Host addedHost;
	private HostTable hostTable;
	private boolean seeded;
	private boolean returnHostTable;

	/**
	 * @param addedHost the addedHost to set
	 */
	public void setAddedHost(Host addedHost) {
		this.addedHost = addedHost;
	}

	/**
	 * @return the addedHost
	 */
	public Host getAddedHost() {
		return addedHost;
	}

	/**
	 * @param returnHostTable the returnHostTable to set
	 */
	public void setReturnHostTable(boolean returnHostTable) {
		this.returnHostTable = returnHostTable;
	}

	/**
	 * @return the returnHostTable
	 */
	public boolean isReturnHostTable() {
		return returnHostTable;
	}

	/**
	 * @param seeded the seeded to set
	 */
	public void setSeeded(boolean seeded) {
		this.seeded = seeded;
	}

	/**
	 * @return the seeded
	 */
	public boolean isSeeded() {
		return seeded;
	}

	/**
	 * @param hostTable the hostTable to set
	 */
	public void setHostTable(HostTable hostTable) {
		this.hostTable = hostTable;
	}

	/**
	 * @return the hostTable
	 */
	public HostTable getHostTable() {
		return hostTable;
	}
}
