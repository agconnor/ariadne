package co.recloud.ariadne.request;

import co.recloud.ariadne.model.plan.Operator;

public class WorkRequest extends Request{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5028099065055375445L;
	private Operator operator;
	/**
	 * @param operator the operator to set
	 */
	public void setOperator(Operator operator) {
		this.operator = operator;
	}
	/**
	 * @return the operator
	 */
	public Operator getOperator() {
		return operator;
	}
}
