package co.recloud.ariadne.request;

import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;

import co.recloud.ariadne.model.Host;

public class BlockMigrateRequest extends Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4917410447545634177L;

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    private int address;
	private Host primary;
	private Map<String, SortedMap<Long, SortedMap<Long, Map<String, Object>>>> block;

	/**
	 * @param block the block to set
	 */
	public void setBlock(Map<String, SortedMap<Long, SortedMap<Long, Map<String, Object>>>> block) {
		this.block = block;
	}
	/**
	 * @return the block
	 */
	public Map<String, SortedMap<Long, SortedMap<Long, Map<String, Object>>>> getBlock() {
		return block;
	}
	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(Host primary) {
		this.primary = primary;
	}
	/**
	 * @return the primary
	 */
	public Host getPrimary() {
		return primary;
	}
}
