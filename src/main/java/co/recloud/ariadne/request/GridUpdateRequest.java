package co.recloud.ariadne.request;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.store.HostTable;

public class GridUpdateRequest extends Request {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3612536744886595465L;
	private HostTable hostTable;
	private Integer hops;
	private Host sourceHost;
	
	/**
	 * @param hostTable the hostTable to set
	 */
	public void setHostTable(HostTable hostTable) {
		this.hostTable = hostTable;
	}
	/**
	 * @return the hostTable
	 */
	public HostTable getHostTable() {
		return hostTable;
	}
	/**
	 * @param hops the hops to set
	 */
	public void setHops(Integer hops) {
		this.hops = hops;
	}
	/**
	 * @return the hops
	 */
	public Integer getHops() {
		return hops;
	}
	public Host getSourceHost() {
		return sourceHost;
	}
	public void setSourceHost(Host sourceHost) {
		this.sourceHost = sourceHost;
	}
}
