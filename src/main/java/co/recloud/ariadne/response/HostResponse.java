package co.recloud.ariadne.response;

import co.recloud.ariadne.store.HostTable;

public class HostResponse extends Response {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5928886035310019611L;
	private HostTable hostTable;

	/**
	 * @param hostTable the hostTable to set
	 */
	public void setHostTable(HostTable hostTable) {
		this.hostTable = hostTable;
	}

	/**
	 * @return the hostTable
	 */
	public HostTable getHostTable() {
		return hostTable;
	}
}
