package co.recloud.ariadne.response;

import java.util.Map;

public class DataResponse extends Response {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2493612811758848896L;
	private Map<String, Object> data;
	private Long rowTime;

	/**
	 * @param data the data to set
	 */
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	/**
	 * @return the data
	 */
	public Map<String, Object> getData() {
		return data;
	}

	/**
	 * @param rowTime the rowTime to set
	 */
	public void setRowTime(Long rowTime) {
		this.rowTime = rowTime;
	}

	/**
	 * @return the rowTime
	 */
	public Long getRowTime() {
		return rowTime;
	}
}
