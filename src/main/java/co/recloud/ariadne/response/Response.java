package co.recloud.ariadne.response;

import java.io.Serializable;

import co.recloud.ariadne.thread.Main;

public class Response implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3938753052704798036L;
	
	private Long time;
	
	public Response() {
		time = Main.getTime();
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Long time) {
		this.time = time;
	}

	/**
	 * @return the time
	 */
	public Long getTime() {
		return time;
	}

}
