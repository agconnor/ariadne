package co.recloud.ariadne.response;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 4/8/12
 * Time: 9:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class BlockMigrateResponse extends Response {
    public boolean isBlockAccepted() {
        return blockAccepted;
    }

    public void setBlockAccepted(boolean blockAccepted) {
        this.blockAccepted = blockAccepted;
    }

    public int getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(int newAddress) {
        this.newAddress = newAddress;
    }
    private int newAddress;
    private boolean blockAccepted;
}
