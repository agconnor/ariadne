package co.recloud.ariadne.queue;

import co.recloud.ariadne.request.Request;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/16/11
 * Time: 11:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class OutboundRequestQueue {
    private static BlockingQueue<Request> queue = new LinkedBlockingQueue<Request>();

    public static BlockingQueue<Request> getQueue() {
        if(queue == null) {
            queue = new LinkedBlockingQueue<Request>();
        }
        return queue;
    }
}
