package co.recloud.ariadne.queue;

import co.recloud.ariadne.model.logical.Transaction;
import co.recloud.ariadne.request.Request;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/23/11
 * Time: 9:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionQueue {
    private static BlockingQueue<Transaction> queue = new LinkedBlockingQueue<Transaction>();

    public static BlockingQueue<Transaction> getQueue() {
        if(queue == null) {
            queue = new LinkedBlockingQueue<Transaction >();
        }
        return queue;
    }
}
