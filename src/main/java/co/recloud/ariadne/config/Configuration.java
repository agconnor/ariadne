package co.recloud.ariadne.config;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/19/11
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Configuration {
    public static int DATA_BLOCK_SIZE = 8192;
    public static int DATA_CACHE_SIZE = 10485760;
    public static int DATA_FILE_WIDTH = 16;
    public static int DATA_FILE_HEIGHT = 16;
    public static String DATA_FILE_DIR = "/var/ariadne";
    public static long TRANSACTION_BLOCK_SIZE;
    public static long TRANSACTION_CACHE_SIZE;
    public static long TRANSACTION_FILE_WIDTH;
    public static long TRANSACTION_FILE_HEIGHT;
    public static long MEMORY_CACHE_SIZE;
    public static long ADDRESS_TABLE_BLOCK_SIZE;
    public static long ADDRESS_TABLE_CACHE_SIZE;
    public static long ADDRESS_TABLE_FILE_WIDTH;
    public static long ADDRESS_TABLE_FILE_HEIGHT;
    public static int ADDRESS_TABLE_SIZE = 64;
    public static int THRESHOLD = 10;

    public static long OUTBOUND_WORKERS = 10;
    public static long GOSSIP_WORKERS = 5;

    private static long interpretSize(String sizeString) {
        return 0;
    }
}
