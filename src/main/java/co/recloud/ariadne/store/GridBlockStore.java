package co.recloud.ariadne.store;

import com.sun.tools.internal.ws.processor.model.Block;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/21/11
 * Time: 1:43 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GridBlockStore<T> {
    public T readBlock(int x, int y);
    public void writeBlock(int x, int y, T block);
    public byte[] readRawBlock(int x, int y);
    public void writeRawBlock(int x, int y, byte [] block);
    public int[] getOpenBlock();
    public void freeBlock(int x, int y);
}
