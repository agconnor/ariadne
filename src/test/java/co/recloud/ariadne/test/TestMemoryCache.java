package co.recloud.ariadne.test;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import co.recloud.ariadne.store.MemoryCache;

public class TestMemoryCache extends TestCase {

	public void testCache() {
		MemoryCache cache = new MemoryCache(8L);
		Map<String, Object> row1 = new HashMap<String, Object>();
		row1.put("col", "AA");
		Map<String, Object> row2 = new HashMap<String, Object>();
		row2.put("col", "BB");
		Map<String, Object> row3 = new HashMap<String, Object>();
		row3.put("col", "CC");
		Map<String, Object> row4 = new HashMap<String, Object>();
		row4.put("col", "DD");
		Map<String, Object> row5 = new HashMap<String, Object>();
		row5.put("col", "EE");
		cache.put("row1", 1L, row1);
		cache.put("row2", 1L, row2);
		cache.put("row3", 1L, row3);
		cache.put("row4", 1L, row4);
		cache.put("row5", 1L, row5);
		assertEquals(true, cache.get("row1", 1L) == null);
		assertEquals(true, cache.get("row2", 1L) != null);
		assertEquals(true, cache.get("row2", 1L).get("col").equals("BB"));
		cache.put("row1", 1L, row1);
		assertEquals(true, cache.get("row3", 1L) == null);
	}
}
