package co.recloud.ariadne.test;

import co.recloud.ariadne.model.Host;
import junit.framework.TestCase;

public class TestHost extends TestCase {
	public void testTokenGen() {
		Host a = new Host();
		a.generateToken();
		Host b = new Host();
		b.generateToken();
		assertEquals(false, a.getToken().equals(b.getToken()));
	}
}
