package co.recloud.ariadne.test;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.model.Location;
import co.recloud.ariadne.store.HostTable;
import junit.framework.TestCase;


public class TestHostTable extends TestCase {
	
	public void testAddHost() {
		HostTable ht = new HostTable();
		Host host = new Host();
		host.setHostName("127.0.0.1");
		host.setPort(1000);
		host.generateToken();
		Host host2 = new Host();
		host2.setHostName("127.0.0.1");
		host2.setPort(2000);
		host2.generateToken();
		Host host3 = new Host();
		host3.setHostName("10.1.1.10");
		host3.setPort(1000);
		host3.generateToken();
		ht.addHost(host);
		ht.addHost(host2);
		ht.addHost(host3);
		assertEquals(3, ht.getTokenRing().size());
		Host host4 = new Host();
		host4.setHostName("10.1.1.10");
		host4.setPort(1000);
		host4.setToken(host3.getToken());
		ht.addHost(host4);
		assertEquals(3, ht.getTokenRing().size());
	}
	
	public void testSetLocalHost() {
		HostTable ht = new HostTable();
		ht.setLocalhost("127.0.0.1", 1000, 0);
		assertEquals(1, ht.getTokenRing().size());
		Host host2 = new Host();
		host2.setHostName("127.0.0.1");
		host2.setPort(2000);
		host2.generateToken();
		Host host3 = new Host();
		host3.setHostName("10.1.1.10");
		host3.setPort(1000);
		host3.generateToken();
		ht.addHost(host2);
		ht.addHost(host3);
		assertEquals(3, ht.getTokenRing().size());
		assertEquals(ht.getLocalhost(), ht.getLocalhost());
	}
	
	public void testGetCoReplica() {
		HostTable ht = new HostTable();
		Location localLocation = new Location();
		localLocation.setX(0L);
		localLocation.setY(0L);
		Host host2 = new Host();
		host2.setHostName("localhost");
		host2.setPort(2);
		host2.generateToken();
		Location location2 = new Location();
		location2.setX(1L);
		location2.setY(0L);
		host2.setLocation(location2);
		ht.setLocalhost("localhost", 1, 0);
		Host localhost  = ht.getLocalhost();
		ht.addHost(host2);
		ht.mapLocationToHost(localLocation, localhost);
		ht.mapLocationToHost(location2, host2);
		ht.setWidth(2L);
		ht.setHeight(1L);
		Host replica = ht.getCoReplica(localhost, host2);
		assertEquals(true, localLocation.equals(replica.getLocation()));
	}
	
	public void testGenerateGrid() {
		HostTable ht = new HostTable();
		ht.setLocalhost("localhost", 0, 0);
		ht.updateFreeHosts();
		ht.expandGrid();
		for(int i = 1; i < 100; i++) {
			Host host = new Host();
			host.setHostName("localhost");
			host.setPort(i);
			host.generateToken();
			ht.addHost(host);
			ht.updateFreeHosts();
			ht.expandGrid();
			for(long x = 0; x < ht.getWidth(); x++) {
				for(long y = 0; y < ht.getHeight(); y++) {
					Location location = new Location();
					location.setX(x);
					location.setY(y);
					assertEquals(true, ht.getHostAtLocation(location) != null && ht.getHostAtLocation(location).getToken() != null);
					assertEquals(true, ht.getCoReplica(ht.getLocalhost(), ht.getLocalhost()) != null);
				}
			}
		}
	}
	
	public void testIsReplica() {
		HostTable ht = new HostTable();
		Location localLocation = new Location();
		localLocation.setX(0L);
		localLocation.setY(0L);
		
		Host host2 = new Host();
		host2.generateToken();
		host2.setHostName("localhost");
		host2.setPort(1);
		Location location2 = new Location();
		location2.setX(1L);
		location2.setY(0L);
		host2.setLocation(location2);
		
		Host host3 = new Host();
		host3.generateToken();
		host3.setHostName("localhost");
		host3.setPort(1);
		Location location3 = new Location();
		location3.setX(0L);
		location3.setY(1L);
		host3.setLocation(location3);
		
		ht.setLocalhost("localhost", 0, 0);
		ht.addHost(host2);
		ht.addHost(host3);
		ht.setWidth(2L);
		ht.setHeight(2L);
		ht.mapLocationToHost(localLocation, ht.getLocalhost());
		ht.mapLocationToHost(location2, host2);
		ht.mapLocationToHost(location3, host3);
		
		Location l1 = new Location();
		l1.setX(0L);
		l1.setY(0L);
		Location l2 = new Location();
		l2.setX(1L);
		l2.setY(0L);
		Location l3 = new Location();
		l3.setX(0L);
		l3.setY(1L);
		assertEquals(true, ht.isReplica(l1));
		assertEquals(true, ht.isReplica(l2));
		assertEquals(true, ht.isReplica(l3));
	}
	
	public void testMerge() {
		HostTable ht1 = new HostTable();
		ht1.setLocalhost("localhost", 1, 0);
		HostTable ht2 = new HostTable();
		ht2.setLocalhost("localhost", 2, 1024);
		ht1.merge(ht2);
		ht2.merge(ht1);
		if(!ht2.expandGrid()) {
			assertEquals(true, false);
		}
		ht1.merge(ht2);
		verifyLocations(ht1);
		verifyLocations(ht2);
		HostTable ht3 = new HostTable();
		ht3.setLocalhost("localhost", 3, 2000);
		ht1.merge(ht3);
		ht2.merge(ht1);
		ht3.merge(ht1);
		if(ht3.expandGrid()) {
			assertEquals(true, false);
		}
		verifyLocations(ht1);
		verifyLocations(ht2);
		verifyLocations(ht3);
		HostTable ht4 = new HostTable();
		ht4.setLocalhost("localhost", 4, 3000);
		ht1.merge(ht4);
		ht2.merge(ht1);
		ht3.merge(ht1);
		ht4.merge(ht1);
		if(!ht4.expandGrid()) {
			assertEquals(true, false);
		} else {
			ht1.merge(ht4);
			ht2.merge(ht4);
			ht3.merge(ht4);
			verifyLocations(ht1);
			verifyLocations(ht2);
			verifyLocations(ht3);
			verifyLocations(ht4);
		}
	}
	
	private void verifyLocations(HostTable ht1) {
		assertEquals(ht1.getHostsInService().size(), ht1.getWidth() * ht1.getHeight());
		assertEquals(ht1.getFreeHosts().size() + ht1.getHostsInService().size(), ht1.getTokenRing().size());
		for(Long x : ht1.getLocationToHost().keySet()) {
			for(Long y : ht1.getLocationToHost().get(x).keySet()) {
				assertEquals(true, x >= 0L && x < ht1.getWidth() && y >= 0L && y < ht1.getHeight());
			}
		}
		for(long x = 0; x < ht1.getWidth(); x++) {
			for(long y = 0; y < ht1.getHeight(); y++) {
				Location location = new Location();
				location.setX(x);
				location.setY(y);
				assertEquals(true, ht1.getHostAtLocation(location) != null && ht1.getHostAtLocation(location).getToken() != null);
				assertEquals(true, ht1.getHostAtLocation(location).getLocation().equals(location));
			}
		}
		if(ht1.getLocalhost().getLocation() != null) {
			assertEquals(true, ht1.getHostAtLocation(ht1.getLocalhost().getLocation()).equals(ht1.getLocalhost()));
		}
	}
}
