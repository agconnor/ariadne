package co.recloud.ariadne.test;

import co.recloud.ariadne.model.Host;
import co.recloud.ariadne.store.AddressTable;
import co.recloud.ariadne.store.HostTable;
import junit.framework.TestCase;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 3/18/12
 * Time: 7:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestAddressTable extends TestCase {
    @Override
    public void setUp() {
        HostTable ht = HostTable.getInstance();
        Host h1 = new Host();
        h1.setHostName("l1");
        h1.setPort(1000);
        h1.setToken(0L);
        ht.addHost(h1);
        Host h2 = new Host();
        h2.setHostName("l2");
        h2.setPort(2000);
        h2.setToken(0x3fffffffL);
        ht.addHost(h2);
        Host h3 = new Host();
        h3.setHostName("l3");
        h3.setPort(2000);
        h3.setToken(0x5fffffffL);
        ht.addHost(h3);
        Host h4 = new Host();
        h4.setHostName("l4");
        h4.setPort(2000);
        h4.setToken(0x7fffffffL);
        ht.addHost(h4);
        ht.updateFreeHosts();
        ht.expandGrid();
    }
    public void testResolve() {
        AddressTable at = AddressTable.getInstance();
        assertNotNull(at.resolve("columnFamily", "testKey"));
    }
    
    public void testRemap() {
        AddressTable at = AddressTable.getInstance();
        int code = "test:test".hashCode();
        at.remap(code, 0x80000000);
        assertEquals("l1", at.resolve("test", "test").getHostName());
    }
}
