package co.recloud.ariadne.test;

import co.recloud.ariadne.store.FileStore;
import junit.framework.TestCase;

import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 11/20/11
 * Time: 2:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class TestFileStore extends TestCase {
    public void testFileStore() {
        FileStore<String> store = new FileStore<String>(new File("/tmp/testfile3"), 16, 4, 4);
        String l1 = "I live in 0,0";
        String l2 = "What's for dinner? I want pierogies!";
        String l3 = "It's in the bathtub with the reef";
        store.writeBlock(0,0,l1);
        store.writeBlock(1,1,l2);
        store.writeBlock(0,1,l3);
        assertEquals(l1, store.readBlock(0,0));
        assertEquals(l2, store.readBlock(1,1));
        assertEquals(l3, store.readBlock(0,1));
    }
}
