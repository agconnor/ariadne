package co.recloud.ariadne.test;

import co.recloud.ariadne.list.SelfOrganizingList;
import junit.framework.TestCase;

/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 3/18/12
 * Time: 4:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestSelfOrganizingList extends TestCase {
    public void testList2() {
        SelfOrganizingList list = new SelfOrganizingList(2);
        list.lookup(1);
        list.lookup(2);
        list.lookup(3);
        assertFalse(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        
        list.lookup(4);
        assertFalse(list.contains(2));
    }

    public void testList50() {
        SelfOrganizingList<Integer> list = new SelfOrganizingList<Integer>(50);
        for (int i = 0; i < 600; i++) {
            list.lookup(i % 60);
        }
        assertEquals(list.size(), 50);
        for (int i : list) {
            assertTrue(list.contains(i));
        }
    }
}
