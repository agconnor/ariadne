package co.recloud.ariadne.test;

import java.util.HashMap;
import java.util.Map;

import co.recloud.ariadne.model.Location;
import co.recloud.ariadne.store.DataStore;
import junit.framework.TestCase;

public class TestDataStore extends TestCase {
	private DataStore store;
	public TestDataStore() {
        Location myLocation = new Location();
        myLocation.setX(0L);
        myLocation.setY(0L);
		store = new DataStore(myLocation);
	}
	
	public void testGet() {
		Map<String, Object> row = new HashMap<String, Object>();
		row.put("testcol", "test");
		store.put("test", "test", "test", 1L, 1L, row);
		Map<String, Object> storedRow = store.get("test", "test", "test", 3L);
		assertEquals(true, storedRow != null);
		assertEquals(true, row.get("testcol").equals(storedRow.get("testcol")));
		storedRow = store.get("test", "test", "test", 1L);
		assertEquals(true, storedRow != null);
		assertEquals(true, row.get("testcol").equals(storedRow.get("testcol")));
	}
}
