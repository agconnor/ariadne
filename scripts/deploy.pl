#!/usr/bin/perl

$listPath = $ARGV[0];
$identityPath = $ARGV[1];
$maxX = $ARGV[2];
$maxY = $ARGV[3];

open(LIST, $listPath);
@hostUrls = <LIST>;
close(LIST);
chomp(@hostUrls);

foreach(@hostUrls) {
    my($url, $x, $y) = split;
    print "Depoying $x $y on $url...\n";
#    system("scp -i $identityPath configure.pl launch.pl launch-primary.pl etc_hosts bootstrap.pl ../target/ariadne.war $url:~/");
    system("scp -i $identityPath configure.pl launch.pl launch-primary.pl etc_hosts bootstrap.pl ../cql/* $url:~/");
    system("ssh -t -i $identityPath $url 'sudo perl bootstrap.pl $x $y $maxX $maxY' ");
}

foreach(@hostUrls) {
    my($url, $x, $y) = split;
    print "Launching Ariadne on $x $y on $url...\n";
    system("ssh -t -i $identityPath $url 'sudo perl launch.pl grid $x $y $maxX $maxY' ");
}