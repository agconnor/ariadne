#!/usr/bin/perl

$hostPrefix = $ARGV[0];
@location = ($ARGV[1], $ARGV[2]);

$maxX = $ARGV[3];
$maxY = $ARGV[4];

$localHostName = $hostPrefix."_".$location[0]."_".$location[1];

print "Configuring ".$localHostName." at location ".$location[0].", ".$location[1]."\n";

$baseThriftPort = 10000;

$baseJmxPort = 11000;

$baseCommandPort = 12000;

@seedList = ();
@thriftPortList = ();
@jmxPortList = ();
@commandPortList = ();

# minus 2, because are starting from a 1-offset and need to convert to 0-offset
for($o = -2; $o < 1; $o++) {
    my $modX = (($location[0] + $o) % $maxX) + 1;
    my $modY = (($location[1] + $o) % $maxY) + 1;
    my $hostName = $hostPrefix."_".$location[0]."_".$modY;
    my $hostId = $location[0] * $maxY + $modY;
    push(@seedList, $hostName);
    push(@thriftPortList, $baseThriftPort + $hostId);
    push(@jmxPortList, $baseJmxPort + $hostId);
    push(@commandPortList, $baseCommandPort + $hostId);
    if($o != -1) {
        $hostName = $hostPrefix."_".$modX."_".$location[1];
        $hostId = $location[0] * $maxY + $modY;
        push(@seedList, $hostName);
        push(@thriftPortList, $baseThriftPort + $hostId);
        push(@jmxPortList, $baseJmxPort + $hostId);
        push(@commandPortList, $baseCommandPort + $hostId);
    }
}

print "Configuring AddressTable\n";

open(CONF, "/usr/share/cassandra/conf/cassandra.yaml");
@confLines = <CONF>;
close(CONF);
$conf = join('', @confLines);
$conf =~ s/127.0.0.1/grid_1_1/gm; #Change the seed -- grid_1_1 will always exist
$conf =~ s/localhost/$localHostName/gm;
open(CONF, ">", "/usr/share/cassandra/conf/cassandra.yaml");
print CONF $conf;
close(CONF);

foreach($i = 0; $i < scalar(@seedList); $i++) {
    my($seedName) = $seedList[$i];
    my($thriftPort) = $thriftPortList[$i];
    my($jmxPort) = $jmxPortList[$i];
    my($commandPort) = $commandPortList[$i];
    print "Configuring $seedName\n";
    system("rm -rf /var/lib/ariadne/$seedName");
    system("mkdir -p /var/lib/ariadne/$seedName");
    system("mkdir -p /usr/share/ariadne/$seedName");
    system("mkdir -p /usr/share/ariadne/$seedName");
    system("mkdir -p /usr/share/ariadne/$seedName/conf");
    system("cp -rf /usr/share/cassandra/conf /usr/share/ariadne/$seedName/");
    open(CONF, "/usr/share/ariadne/$seedName/conf/cassandra.yaml");
    @confLines = <CONF>;
    close(CONF);
    
    $conf = join('', @confLines);
    $conf =~ s/localhost/$localHostName/gm;
    $conf =~ s/127.0.0.1/$seedName/gm;
    $conf =~ s/9160/$thriftPort/gm;
    $conf =~ s/7000/$commandPort/gm;
    $conf =~ s/Test Cluster/Grid $seedName/gm;
    $conf =~ s:/var/lib/cassandra:/var/lib/ariadne/$seedName:gm;
    
    open(CONF, ">", "/usr/share/ariadne/$seedName/conf/cassandra.yaml");
    print CONF $conf;
    close(CONF);
    
    open(JMX, "/usr/share/ariadne/$seedName/conf/cassandra-env.sh");
    @jmx = <JMX>;
    close JMX;
    
    $jmx = join('', @jmx);
    $jmx =~ s/7199/$jmxPort/gm;
    
    open(JMX, ">", "/usr/share/ariadne/$seedName/conf/cassandra-env.sh");
    print JMX $jmx;
    close(JMX);
}
