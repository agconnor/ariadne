#!/usr/bin/perl

$x = $ARGV[0];
$y = $ARGV[1];
$maxX = $ARGV[2];
$maxY = $ARGV[3];

#Install hosts
system('cp -f etc_hosts /etc/hosts');
#Get Tomcat
system('wget http://archive.apache.org/dist/tomcat/tomcat-6/v6.0.29/bin/apache-tomcat-6.0.29.tar.gz');
system('tar -xzf apache-tomcat-6.0.29.tar.gz');
system('cp -rf apache-tomcat-6.0.29 /usr/share/tomcat');
system('cp ariadne.war /usr/share/tomcat/webapps');
system('/usr/share/tomcat/startup.sh');

#Get Cassandra
system('wget http://apache.mirrors.tds.net//cassandra/0.8.4/apache-cassandra-0.8.4-bin.tar.gz');
system('tar -xzf apache-cassandra-0.8.4-bin.tar.gz');
system('cp -r apache-cassandra-0.8.4 /usr/share/cassandra');
system('rm -rf /var/lib/cassandra');

#Configure Cassandra and Ariadne
system('killall -9 java');
system('chmod 755 configure.pl');
system('chmod 755 launch-primary.pl');
system('chmod 755 launch.pl');
system("./configure.pl grid $x $y $maxX $maxY");

#Launch Primary
system("./launch-primary.pl grid $x $y $maxX $maxY");