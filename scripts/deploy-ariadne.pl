#!/usr/bin/perl

$listPath = $ARGV[0];
$identityPath = $ARGV[1];

open(LIST, $listPath);
@hostUrls = <LIST>;
close(LIST);
chomp(@hostUrls);

foreach(@hostUrls) {
    my($url, $x, $y) = split;
    print "Depoying $x $y on $url...\n";
    system("scp -i $identityPath ../target/ariadne.war $url:~/");
#    system("ssh -t -i $identityPath $url 'wget ' ");
    system("ssh -t -i $identityPath $url 'sudo cp ariadne.war /usr/share/tomcat/webapps/' ");
}